package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.InvestmentFrame;
import model.BankAccount;

public class Controller {
	private static final double INITIAL_BALANCE = 1000;
	public InvestmentFrame gui;
	public BankAccount account;
	
	public Controller() {
		this.account = new BankAccount(INITIAL_BALANCE);
		this.gui = new InvestmentFrame();
		ActionListener listener = new AddInterestListener();
		this.gui.setListener(listener);
	}
	
	class AddInterestListener implements ActionListener {
		public void actionPerformed(ActionEvent event)
		{
			double rate = gui.getRate();
			double interest = account.calculateInterest(rate);
			account.deposit(interest);
			gui.setResult("" + account.getBalance());
		}
	}
	
	public static void main(String[] args){
		new Controller();
	   }
}
/**
object �ͧ  BankAccount �� attribute � controller �����������ö���¡�� attribute �ͧ  class BankAccount
�¡��  new object account �������ö�觤�Ңͧ Initial balance ��ѧ class BankAccount ���͹�令ӹǳ��� 
��� new object gui �����������ö���¡�� attribute �ͧ  class InvestmentFrame �� 
*/
