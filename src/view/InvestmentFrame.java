package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;

import javax.swing.ActionMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;
import control.Controller;


public class InvestmentFrame extends JFrame{
	   private static final int FRAME_WIDTH = 450;
	   private static final int FRAME_HEIGHT = 100;

	   private static final double DEFAULT_RATE = 5;   
	   private static final double INITIAL_BALANCE = 1000;
	   
	   private JLabel rateLabel;
	   private JTextField rateField;
	   private JButton button;
	   private JLabel resultLabel;
	   private JPanel panel;
	   private BankAccount account;
	   
	   public InvestmentFrame (){
		   	  account = new BankAccount(INITIAL_BALANCE);

		      // Use instance variables for components 
		      resultLabel = new JLabel("balance: " + account.getBalance());

		      // Use helper methods
		      createTextField();
		      createButton();
		      createPanel();
		      
		      setVisible(true);
		      setSize(FRAME_WIDTH, FRAME_HEIGHT);
		   }

		   private void createTextField()
		   {
		      rateLabel = new JLabel("Interest Rate: ");

		      final int FIELD_WIDTH = 10;
		      rateField = new JTextField(FIELD_WIDTH);
		      rateField.setText("" + DEFAULT_RATE);
		   }
		   
		   public double getRate(){
			  double rate = Double.parseDouble(rateField.getText());
			  return rate;
		   }
		   
		   public void setResult (String str){
			  resultLabel.setText("balance: "+str);
		   }
		   
		   public void setListener(ActionListener listener) {
			  button.addActionListener(listener);
		   }

		   private void createButton()
		   {
		      button = new JButton("Add Interest");
		   }

		   private void createPanel()
		   {
		      panel = new JPanel();
		      panel.add(rateLabel);
		      panel.add(rateField);
		      panel.add(button);
		      panel.add(resultLabel);      
		      add(panel);
		   } 
}
